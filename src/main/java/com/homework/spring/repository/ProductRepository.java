package com.homework.spring.repository;

import com.homework.spring.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {
    List<Product> products = new ArrayList<>();

    {
        products.add(new Product(001,10,"Coca",5,"This is Coca-Cola"));
        products.add(new Product(002,20,"Pepsi",10,"This is Pepsi"));
        products.add(new Product(003,30,"Sting",8,"This is Sting"));
        products.add(new Product(004,40,"Water",10,"This is Water"));
        products.add(new Product(005,50,"ABC",25,"This is ABC"));
        products.add(new Product(006,60,"Angkor",15,"This is Angkor"));
        products.add(new Product(007,70,"Cambodia",11,"This is Cambodia"));
    }

    public List<Product> findAll() {
        return products;
    };

    public Product findOne(int id) {
        for( int i = 0; i < products.size(); i++) {
            if(products.get(i).getId() == id) {
                return products.get(i);
            }
        }
        return null;
    }

    public Boolean add(Product product) {
        return products.add(product);
    }

    public Boolean update(Product product) {
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).getId() == product.getId()) {
                products.set(i, product);
                return true;
            }
        }
        return false;
    }

    public Boolean delete(int id) {
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).getId() == id) {
                products.remove(i);
                return true;
            }
        }
        return false;
    }

}
