package com.homework.spring.repository;

import com.homework.spring.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student> students = new ArrayList<>();

    public List<Student> findAll() {
        students.add(new Student(001,"Sok"));
        students.add(new Student(002,"Steven"));
        students.add(new Student(003,"Jruy"));
        return students;
    }
}
