package com.homework.spring.service;

import com.homework.spring.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> findAll();
}
