package com.homework.spring.service;

import com.homework.spring.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    Product findOne(int id);
    Boolean add(Product product);
    Boolean update(Product product);
    Boolean delete(int id);
}
