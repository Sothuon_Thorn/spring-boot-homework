package com.homework.spring.service.imp;

import com.homework.spring.model.Product;
import com.homework.spring.repository.ProductRepository;
import com.homework.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findOne(int id) {
        return productRepository.findOne(id);
    }

    @Override
    public Boolean add(Product product) {
        return productRepository.add(product);
    }

    @Override
    public Boolean update(Product product) {
        return productRepository.update(product);
    }

    @Override
    public Boolean delete(int id) {
        return productRepository.delete(id);
    }
}
