package com.homework.spring.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeacherController {

    @GetMapping("/teacher")
    public String teacher() {
        return "Hello Teacher Spring Boot! How are you doing?";
    }

    @GetMapping("/student")
    public String student() {
        return "Welcome to My Spring Boot Homework!";
    }
}
