package com.homework.spring.controllers;

import com.homework.spring.model.Product;
import com.homework.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/products")
public class ProductController {
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping("")
    public ResponseEntity<Map<String, Object>> getAll() {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Records Found");
        map.put("data", productService.findAll());
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<>(map,HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Map<String, Object>> getOne(@PathVariable int id) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Record Found");
        map.put("data", productService.findOne(id));
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Map<String, Object>> create(@RequestBody Product product) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Data Created");
        map.put("data", productService.add(product));
        map.put("status", HttpStatus.CREATED);
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Object>> update(@PathVariable int id, @RequestBody Product product) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Data Updated");
        map.put("data", productService.update(product));
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable int id) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Data Deleted");
        map.put("data", productService.delete(id));
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
